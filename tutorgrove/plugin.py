import copy
from importlib import resources

from tutor import hooks, config as tutor_config
try:
    from tutormfe.hooks import MFE_APPS
    from tutormfe.plugin import iter_mfes, get_mfes
except ImportError:
    MFE_APPS = None
    iter_mfes = lambda: []

try:
    from tutorforum.hooks import FORUM_ENV
except ImportError:
    FORUM_ENV = None

from . import commands
from .__about__ import __version__

################# Autoscaling
# Here comes the default common settings for the autoscaling. Some resources
# should not request a specific resources, but rather use as much/as few as
# possible. This is the case for the CPU and memory in the case of workers.
# Hence workers has no "*_MEMORY_REQUEST" but only "*_MEMORY_LIMIT" settings.
# The LMS and CMS (non-worker) memory limits are set to have a small amount of
# overcommitment. This is to prevent the LMS and CMS from being slow for
# learners and instructors, though workers may lag a bit behind with tasks.
CMS_MEMORY_REQUEST_MB = 350
CMS_MAX_REPLICAS = 4
CMS_WORKER_MEMORY_REQUEST_MB = 750

LMS_MEMORY_REQUEST_MB = 350
LMS_MAX_REPLICAS = 4
LMS_WORKER_MEMORY_REQUEST_MB = 750


################# Configuration
config = {
    # Add here your new settings
    "defaults": {
        "VERSION": __version__,

        # If you want to run the MFE's behind a CDN
        # This is the origin that your CDN will proxy to.
        "MFE_CDN_ORIGIN": "",

        # Set the static URL for LMS/CMS respectively. Leave empty for default.
        "LMS_STATIC_URL": "",
        "CMS_STATIC_URL": "",

        # Set spec.revisionHistoryLimit for any
        # of the default deployments created by tutor.
        "REVISION_HISTORY_LIMIT": 10,

        # Set to True to host static files on S3
        "ENABLE_S3_STATIC_FILES": False,

        # The bucket name to use for S3 static files
        "S3_STATIC_FILES_BUCKET_NAME": "{{S3_STORAGE_BUCKET}}",

        # Enable/disable the Celery beat pods
        "ENABLE_CELERY_BEAT": False,

        # Enable/disable shared Elasticsearch
        "ENABLE_SHARED_ELASTICSEARCH": False,

        "MAINTENANCE_S3_BUCKET_ROOT_URL": "",
        "SERVER_ERROR_S3_BUCKET_ROOT_URL": "",

        # If the comprehensive theme used by the Grove instance contains a npm brand package,
        # set this variable to the name of the package so that it gets installed in the MFE environment.
        "COMPREHENSIVE_THEME_BRAND_PACKAGE_NAME": "",

        "CMS_MEMORY_REQUEST": f"{CMS_MEMORY_REQUEST_MB}Mi",
        "LMS_MEMORY_REQUEST": f"{LMS_MEMORY_REQUEST_MB}Mi",

        # Kubernetes Pod Disruption Budgets -- if set to -1 (not a string), then PDB is disabled
        # Max unavailable is set to 0 to prevent any downtime during upgrades, however this may
        # cause issues in case of many replicas. If the autoscaling is set to other than the
        # default, then the max unavailable sould be considered to be set to a higher value.
        "CADDY_PDB_MAX_UNAVAILABLE": 0,
        "LMS_PDB_MAX_UNAVAILABLE": 0,
        "CMS_PDB_MAX_UNAVAILABLE": 0,
        "LMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "CMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "FORUM_PDB_MAX_UNAVAILABLE": 0,
        "MFE_PDB_MAX_UNAVAILABLE": 0,

        # Kubernetes autoscaling settings
        "CADDY_AUTOSCALING": False,
        "CADDY_AVG_CPU": 0,
        "CADDY_AVG_MEMORY": 0,
        "CADDY_CPU_LIMIT": 0,
        "CADDY_CPU_REQUEST": 0,
        "CADDY_MEMORY_LIMIT": 0,
        "CADDY_MEMORY_REQUEST": 0,
        "CADDY_MIN_REPLICAS": 1,
        "CADDY_MAX_REPLICAS": 1,

        "CMS_AUTOSCALING": True,
        "CMS_AVG_CPU": 300,
        "CMS_AVG_MEMORY": "",
        "CMS_CPU_LIMIT": 1,
        "CMS_CPU_REQUEST": 0.25,
        "CMS_MAX_REPLICAS": CMS_MAX_REPLICAS,
        "CMS_MEMORY_LIMIT": f"{CMS_MEMORY_REQUEST_MB * 4}Mi",
        "CMS_MIN_REPLICAS": 1,

        "CMS_WORKER_AUTOSCALING": True,
        "CMS_WORKER_AVG_CPU": 400,
        "CMS_WORKER_AVG_MEMORY": "", # Disable memory-based autoscaling
        "CMS_WORKER_CPU_LIMIT": 1,
        "CMS_WORKER_CPU_REQUEST": 0.175,
        "CMS_WORKER_MAX_REPLICAS": CMS_MAX_REPLICAS * 1.5,
        "CMS_WORKER_MEMORY_LIMIT": f"{CMS_WORKER_MEMORY_REQUEST_MB * 4}Mi",
        "CMS_WORKER_MEMORY_REQUEST": f"{CMS_WORKER_MEMORY_REQUEST_MB}Mi",
        "CMS_WORKER_MIN_REPLICAS": 1,

        "FORUM_AUTOSCALING": False,
        "FORUM_AVG_CPU": 0,
        "FORUM_AVG_MEMORY": 0,
        "FORUM_CPU_LIMIT": 0,
        "FORUM_CPU_REQUEST": 0,
        "FORUM_MEMORY_LIMIT": 0,
        "FORUM_MEMORY_REQUEST": 0,
        "FORUM_MIN_REPLICAS": 1,
        "FORUM_MAX_REPLICAS": 1,

        "LMS_AUTOSCALING": True,
        "LMS_AVG_CPU": 300,
        "LMS_AVG_MEMORY": "",
        "LMS_CPU_LIMIT": 1,
        "LMS_CPU_REQUEST": 0.25,
        "LMS_MAX_REPLICAS": LMS_MAX_REPLICAS,
        "LMS_MEMORY_LIMIT": f"{LMS_MEMORY_REQUEST_MB * 4}Mi",
        "LMS_MIN_REPLICAS": 1,

        "LMS_WORKER_AUTOSCALING": True,
        "LMS_WORKER_AVG_CPU": 400,
        "LMS_WORKER_AVG_MEMORY": "", # Disable memory-based autoscaling
        "LMS_WORKER_CPU_LIMIT": 1,
        "LMS_WORKER_CPU_REQUEST": 0.175,
        "LMS_WORKER_MAX_REPLICAS": LMS_MAX_REPLICAS * 1.5,
        "LMS_WORKER_MEMORY_LIMIT": f"{LMS_WORKER_MEMORY_REQUEST_MB * 4}Mi",
        "LMS_WORKER_MEMORY_REQUEST": f"{LMS_WORKER_MEMORY_REQUEST_MB}Mi",
        "LMS_WORKER_MIN_REPLICAS": 1,

        "CADDY_VOLUME_SIZE": "",
        "ELASTICSEARCH_VOLUME_SIZE": "",
        "REDIS_VOLUME_SIZE": "",

        # Enable automatic waffle flag creation
        #
        # Example:
        #   WAFFLE_FLAGS: [
        #       {
        #           "name": "course_experience.enable_about_sidebar_html",
        #           "everyone": true,
        #       },
        #   ]
        "WAFFLE_FLAGS": [],

        # Additional settings
        "COMMON_ENV_FEATURES": "",
        "COMMON_SETTINGS": "",

        "LMS_ENV": "",
        "LMS_ENV_FEATURES": "",
        "LMS_PRODUCTION_SETTINGS": "",

        "CMS_ENV": "",
        "CMS_ENV_FEATURES": "",
        "CMS_PRODUCTION_SETTINGS": "",

        "MFE_LMS_COMMON_SETTINGS": "",

        # Open edX auth settings
        "OPENEDX_AUTH": "",

        # Enable cron jobs
        #
        # Example:
        #   CRON_JOBS: [
        #       {
        #           "name": "run-aggregator-service",
        #           "schedule": "*/10 * * * *",
        #           "script": "./manage.py cms run_aggregator_service"
        #       },
        #       {
        #           "name": "hello-world",
        #           "schedule": "*/20 * * * *",
        #           "script": 'echo "Hello world"',
        #       }
        #   ]
        "CRON_JOBS": [],
        # Additional domains that you would like to add
        # A list of dictionaries with keys `proxy` and `domain`
        # [{"domain": "example.com", "proxy": "lms:8000" }]
        "ADDITIONAL_DOMAINS": [],
        # Extra origins/hosts which need to be allowed and
        # whitelisted for CORS
        "ALLOW_EXTRA_ORIGINS": [],
        # Configure redirect rules through Caddy
        "REDIRECTS": [],
        # eox-tenant for multi domain support
        "USE_EOX_TENANT": False,
        # MFEs listed here will be disabled
        "DISABLED_MFES": [],
        # MFE list to be added/updated
        "NEW_MFES": {}
    },
    # Add here settings that don't have a reasonable default for all users. For
    # instance: passwords, secret keys, etc.
    "unique": {},
    # Danger zone! Add here values to override settings from Tutor core or other plugins.
    "overrides": {
        # The list of indexes is defined in:
        # https://github.com/openedx/course-discovery/blob/master/course_discovery/settings/base.py
        # We need to keep a copy of this list so that we can prefix the index names when using shared ES.
        "DISCOVERY_INDEX_OVERRIDES": {
            "course_discovery.apps.course_metadata.search_indexes.documents.course": "{{ ELASTICSEARCH_INDEX_PREFIX }}course",
            "course_discovery.apps.course_metadata.search_indexes.documents.course_run": "{{ ELASTICSEARCH_INDEX_PREFIX }}course_run",
            "course_discovery.apps.course_metadata.search_indexes.documents.learner_pathway": "{{ ELASTICSEARCH_INDEX_PREFIX }}learner_pathway",
            "course_discovery.apps.course_metadata.search_indexes.documents.person": "{{ ELASTICSEARCH_INDEX_PREFIX }}person",
            "course_discovery.apps.course_metadata.search_indexes.documents.program": "{{ ELASTICSEARCH_INDEX_PREFIX }}program",
        }
    },
    "global_defaults": {
        "ELASTICSEARCH_INDEX_PREFIX": "",
        "ELASTICSEARCH_HTTP_AUTH": "",
        "ELASTICSEARCH_CA_CERT_PEM": ""
    }
}


def get_site_configuration(grove_additional_domain: dict) -> dict:
    """
    Check the grove_additional_domain for a `site_configuration`
    key and return it. Otherwise return a default Site Configuration
    for the LMS based on the domain.
    """

    domain = grove_additional_domain["domain"]
    site_configuration = grove_additional_domain.get("site_configuration")

    if site_configuration is not None:
        return site_configuration

    return {
        "SITE_NAME": domain,
        "SESSION_COOKIE_DOMAIN": domain,
        "LMS_BASE": domain,
        "LMS_ROOT": f"https://{domain}",
        "version": 0
    }


hooks.Filters.ENV_TEMPLATE_FILTERS.add_item(
    ("get_site_configuration", get_site_configuration)
)


def merge_dict(base_dict, override):
    """
    Merge two nested dicts.
    """
    if isinstance(base_dict, dict) and isinstance(override, dict):
        for key, value in override.items():
            base_dict[key] = merge_dict(base_dict.get(key, {}), value)
        return base_dict

    return override


def get_base_tenant_configuration(site_config: dict, lms_host: str, enable_https: bool) -> dict:
    """
    Get base tenant configuration for main URL if eox tenant is enabled.
    It makes sure third_party_auth works as expected if enabled.
    To enable third_party_auth set `FEATURES["ENABLE_THIRD_PARTY_AUTH"] = True`
    in common features or tenant settings.
    """
    protocol = "https" if enable_https else "http"
    default = {
        "version": 0,
        "EDNX_TENANT_INSTALLED_APPS": [
            "common.djangoapps.third_party_auth"
        ],
        "LMS_BASE": lms_host,
        "LMS_ROOT_URL": f"{protocol}://{lms_host}",
        "PREVIEW_LMS_BASE": f"preview.{lms_host}",
        "FEATURES": {"PREVIEW_LMS_BASE": f"preview.{lms_host}"},
        "EDNX_USE_SIGNAL": True,
    }
    return {"lms_configs": merge_dict(default, site_config)}


hooks.Filters.ENV_TEMPLATE_FILTERS.add_item(
    ("get_base_tenant_configuration", get_base_tenant_configuration)
)

def get_tenant_configuration(grove_additional_domain: dict, enable_https: bool) -> dict:
    """
    Check the grove_additional_domain for a `site_configuration`
    key and return it. Otherwise return a default Tenant Configuration
    for the LMS based on the domain.
    """

    site_configuration = grove_additional_domain.get("site_configuration", {})
    domain = grove_additional_domain["domain"]
    mfe_domain = grove_additional_domain.get("mfe_domain", None)
    proxy = grove_additional_domain.get("proxy")
    protocol = "https" if enable_https else "http"
    final_config = {}
    if proxy == "lms:8000":
        default_common_config = {
            "ALLOWED_HOSTS": [domain, f"studio.{domain}", f"preview.{domain}"],
            "CMS_BASE": f"studio.{domain}",
            "CORS_ORIGIN_WHITELIST": [f"{protocol}://{domain}", f"{protocol}://studio.{domain}", f"{protocol}://preview.{domain}"],
            "CSRF_TRUSTED_ORIGINS": [domain, f"studio.{domain}", f"preview.{domain}"],
            "EDNX_USE_SIGNAL": True,
            "FEATURES": {"PREVIEW_LMS_BASE": f"preview.{domain}"},
            "HOSTNAME_MODULESTORE_DEFAULT_MAPPINGS": {f"preview.{domain}": "draft-preferred"},
            "IDA_LOGOUT_URI_LIST": [f"{protocol}://studio.{domain}/logout/"],
            "LMS_BASE": domain,
            "LMS_ROOT_URL": f"{protocol}://{domain}",
            "LOGIN_REDIRECT_WHITELIST": [f"studio.{domain}"],
            "PREVIEW_LMS_BASE": f"preview.{domain}",
            "SESSION_COOKIE_DOMAIN": domain,
            "SHARED_COOKIE_DOMAIN": domain,
            "SOCIAL_AUTH_EDX_OAUTH2_PUBLIC_URL_ROOT": f"{protocol}://{domain}",
        }
        if mfe_domain:
            mfe_base = f"{protocol}://{mfe_domain}"
            default_common_config["MFE_CONFIG"] = {
                "BASE_URL": mfe_domain,
                "LMS_BASE_URL": f"{protocol}://{domain}",
                "STUDIO_BASE_URL": f"{protocol}://studio.{domain}",
                "LEARNING_BASE_URL": f"{mfe_base}/learning",
                "LOGIN_URL": f"{protocol}://{domain}/login",
                "LOGOUT_URL": f"{protocol}://{domain}/logout",
                "MARKETING_SITE_BASE_URL": f"{protocol}://{domain}",
                "REFRESH_ACCESS_TOKEN_ENDPOINT":  f"{protocol}://{domain}/login_refresh",
                "FAVICON_URL": f"{protocol}://{domain}/favicon.ico",
                "LOGO_URL":f"{protocol}://{domain}/theming/asset/images/logo.png",
                "LOGO_WHITE_URL":f"{protocol}://{domain}/theming/asset/images/logo.png",
                "LOGO_TRADEMARK_URL":f"{protocol}://{domain}/theming/asset/images/logo.png",
            }
            default_common_config["LEARNING_MICROFRONTEND_URL"] = f"{mfe_base}/learning"
            for app_name, app in iter_mfes():
                if app_name == "authn":
                    default_common_config.update({
                        "AUTHN_MICROFRONTEND_URL": f"{mfe_base}/authn",
                        "AUTHN_MICROFRONTEND_DOMAIN": f"{mfe_domain}/authn",
                    })
                if app_name == "account":
                    default_common_config["ACCOUNT_MICROFRONTEND_URL"] = f"{mfe_base}/account"
                    default_common_config["MFE_CONFIG"]["ACCOUNT_SETTINGS_URL"] = f"{mfe_base}/account"
                if app_name == "course-authoring":
                    default_common_config["COURSE_AUTHORING_MICROFRONTEND_URL"] = f"{mfe_base}/course-authoring"
                if app_name == "discussions":
                    default_common_config["DISCUSSIONS_MICROFRONTEND_URL"] = f"{mfe_base}/discussions"
                if app_name == "gradebook":
                    default_common_config["WRITABLE_GRADEBOOK_URL"] = f"{mfe_base}/gradebook"
                if app_name == "ora-grading":
                    default_common_config["ORA_GRADING_MICROFRONTEND_URL"] = f"{mfe_base}/ora-grading"
                if app_name == "profile":
                    default_common_config["PROFILE_MICROFRONTEND_URL"] = f"{mfe_base}/profile"
                    default_common_config["MFE_CONFIG"]["ACCOUNT_PROFILE_URL"] = f"{mfe_base}/profile"
                if app_name == "communications":
                    default_common_config["COMMUNICATIONS_MICROFRONTEND_URL"] = f"{mfe_base}/communications"
                    default_common_config["MFE_CONFIG"]["SCHEDULE_EMAIL_SECTION"] = True

            default_common_config["CORS_ORIGIN_WHITELIST"] += [f"{mfe_base}"]
            default_common_config["LOGIN_REDIRECT_WHITELIST"] += [f"{mfe_base}"]
            default_common_config["CSRF_TRUSTED_ORIGINS"] += [f"{mfe_domain}"]

            if site_configuration.get("PLATFORM_NAME"):
                default_common_config["SITE_NAME"] = site_configuration.get("PLATFORM_NAME")

        default_lms_config = {"EDNX_TENANT_INSTALLED_APPS": ["common.djangoapps.third_party_auth"]}
        default_config = merge_dict(copy.deepcopy(default_common_config), default_lms_config)
        lms_configuration = merge_dict(default_config, site_configuration)
        final_config = {"lms_configs": lms_configuration, "studio_configs": default_common_config}
    elif proxy == "cms:8000":
        final_config = {"studio_configs": site_configuration}
    return final_config


hooks.Filters.ENV_TEMPLATE_FILTERS.add_item(
    ("get_tenant_configuration", get_tenant_configuration)
)


def is_plugin_loaded(plugin_name: str) -> bool:
    """
    Check if the provided plugin is loaded.
    """

    return plugin_name in hooks.Filters.PLUGINS_LOADED.iterate()


hooks.Filters.ENV_TEMPLATE_VARIABLES.add_item(
    ("is_plugin_loaded", is_plugin_loaded)
)

################# Initialization tasks
tasks_dir = resources.files("tutorgrove") / "templates" / "grove" / "tasks"

for task_file in tasks_dir.glob("*/*"):
    service, task = task_file.parts[-2:]
    hooks.Filters.CLI_DO_INIT_TASKS.add_item((
        service,
        task_file.read_text(encoding='utf-8'),
    ))


################# You don't really have to bother about what's below this line,
################# except maybe for educational purposes :)

# Plugin templates
hooks.Filters.ENV_TEMPLATE_ROOTS.add_item(str(resources.files("tutorgrove") / "templates"))

hooks.Filters.ENV_TEMPLATE_TARGETS.add_items(
    [
        ("grove/build", "plugins"),
        ("grove/apps", "plugins"),
        ("grove/k8s", "plugins"),
    ],
)

# The patches from this plugin should be loaded after all other plugins
# so that the overrides configured for individual Grove instances
# are themselves not overriden by the default patches of other plugins.
# The priority of these patches are therefore set to 100.
for patch_file in (resources.files("tutorgrove") / "patches").glob("*"):
    hooks.Filters.ENV_PATCHES.add_item((
        patch_file.name,
        patch_file.read_text(encoding='utf-8')
    ), priority=100)

# Disable patches in the Dockerfile, because they don't apply cleanly
hooks.Filters.ENV_PATCHES.add_item(
  (
  "openedx-dockerfile-git-patches-default",
  "#"
  )
)


# Load all configuration entries
hooks.Filters.CONFIG_DEFAULTS.add_items(
    [
        (f"GROVE_{key}", value)
        for key, value in config["defaults"].items()
    ]
)

# Defaults for values derived from this config
hooks.Filters.CONFIG_DEFAULTS.add_items(list(config["global_defaults"].items()))

hooks.Filters.CONFIG_UNIQUE.add_items(
    [
        (f"GROVE_{key}", value)
        for key, value in config["unique"].items()
    ]
)

hooks.Filters.CONFIG_OVERRIDES.add_items(list(config["overrides"].items()))

hooks.Filters.CLI_COMMANDS.add_item(commands.maintenance_mode)

if MFE_APPS:
    @hooks.Actions.PROJECT_ROOT_READY.add()
    def _modify_mfes(root: str):
        config = tutor_config.load_minimal(root)
        disabled_mfes = config.get('GROVE_DISABLED_MFES', [])
        new_mfes = config.get('GROVE_NEW_MFES', {})
        get_mfes.cache_clear()

        @MFE_APPS.add()
        def _add_remove_mfe(mfes):
            for key in disabled_mfes:
                if key in mfes:
                    mfes.pop(key)
            mfes = merge_dict(mfes, new_mfes)
            return mfes

if FORUM_ENV:
    @FORUM_ENV.add()
    def _add_forum_env_vars(env_vars):
        env_vars.update(
            {
                "SEARCH_SERVER": "{{ ELASTICSEARCH_SCHEME }}://{{ ELASTICSEARCH_HTTP_AUTH and (ELASTICSEARCH_HTTP_AUTH + '@') }}{{ ELASTICSEARCH_HOST }}:{{ ELASTICSEARCH_PORT }}",
                "ELASTICSEARCH_INDEX_PREFIX": '{{ ELASTICSEARCH_INDEX_PREFIX|default("") }}',
            }
        )
        return env_vars
