grove plugin for `Tutor`_
=========================

This plugin is meant to be used with `Grove`_

It contains patches and commands useful for deployment of instances
using Grove

Here is the list of patches and commands:

1. Run ``create_or_update_site_configuration`` management command during
   lms init. This sets the site configurations for the grove instance.
   The parameters required are ``SITE_CONFIG`` and ``LMS_HOST``. `Site
   Configuration`_

Installation
------------

.. code:: bash

   pip install git+https://gitlab.com/opencraft/dev/tutor-contrib-grove

Usage
-----

.. code:: bash

   tutor plugins enable grove

Cron jobs
---------

This plugin allows you to run cron jobs by defining an array of objects
containing the job schedule and script to execute. The jobs are running using
the edxapp docker image, however not within the LMS/Studio containers.

There are many reasons you may want this behaviour, for example running
`manage.py` commands periodically, like aggregator cleanups. Take the following
config example:

.. code-block:: yaml

    # instance/config.yml
    GROVE_CRON_JOBS:
        - name: "run-aggregator-service"
          schedule: "*/10 * * * *"
          script: "./manage.py lms run_aggregator_service"
        - name: "hello-world"
          schedule: "*/20 * * * *"
          script: 'echo "Hello world"

Please note that only "-" and alphanumeric characters are allowed in name
field. Spaces and underscores are replaced by hypen i.e. "-" automatically.

Autoscaling
-----------

The plugin implements Horizontal Pod Scaling for Caddy, LMS, CMS and their worker pods.

The following fields are supported for Caddy:

:GROVE_CADDY_AUTOSCALING:
   Default: `false`. If `true`, enable autoscaling for Caddy. When disabled, the variables below will be ignored.
:GROVE_CADDY_AVG_CPU:
   The average CPU utilization on a pod before the autoscaler kicks in.
:GROVE_CADDY_AVG_MEMORY:
   The average Memory utilization on a pod before the autoscaler kicks in. We recommend leaving this off unless required.
:GROVE_CADDY_CPU_LIMIT:
   The maximum CPU utilization allowed per pod.
:GROVE_CADDY_CPU_REQUEST:
   When this value is specified, the resources are reserved for the pod.
:GROVE_CADDY_MEMORY_LIMIT:
   The maximum amount of memory allowed per pod.
:GROVE_CADDY_MEMORY_REQUEST:
   Reserve a specific amount of RAM for the pod.
:GROVE_CADDY_MIN_REPLICAS:
   The minimum number of replicas to run.
:GROVE_CADDY_MAX_REPLICAS:
   The maximum number of replicas to run.

The other services are configured similarly, with `CADDY` replaced by the actual service name, eg. `GROVE_LMS_AUTOSCALING`.

Currently supported services are `CADDY`, `CMS`, `CMS_WORKER`, `LMS` and `LMS_WORKER`. Default values are specified `plugin.py`.

Static URL
----------

Grove's AWS provider supports using a CDN for LMS and CMS static assets. To make use of the CDN, this plugin
exposes configuration variables to set the `STATIC_URL` for the LMS and CMS.

These are, respectively.

- `GROVE_LMS_STATIC_URL`
- `GROVE_CMS_STATIC_URL`

In addition when configuring MFE's the MFE origin needs to be configured.

- `GROVE_MFE_CDN_ORIGIN`


Multi-tenancy
-------------

Grove can deploy multi-tenant installations with a different theme for each instance. You can read more about this
in the [Grove documenation](https://grove.opencraft.com/addons/multi-domain-setups/).

License
-------

This software is licensed under the terms of the AGPLv3.

.. _Tutor: https://docs.tutor.overhang.io
.. _Grove: https://grove.opencraft.com
.. _Site Configuration: https://grove.opencraft.com/user-guides/overriding-configuration/#site-configuration

